const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {GenerateSW} = require('workbox-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const pathsToClean = ['build'];

module.exports = {
    entry: {
        index: ['@babel/polyfill', './src/index.js'],
        about: ['./src/components/About/index.js'],
        contact: ['./src/components/Contact/index.js'],
        hire: ['./src/containers/Hire/Hire.js'],
        home: ['./src/components/Home/index.js']
    },
    mode: 'production',
    output: {
        path: path.join(__dirname, '/build'),
        publicPath: '/',
        filename: '[name].[hash].js'
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: Infinity,
            minSize: 0
        }
    },
    performance: {
        maxEntrypointSize: 400000
    },
    resolve: {
        alias: {
            actions: path.resolve(__dirname, 'src/actions/'),
            components: path.resolve(__dirname, 'src/components/'),
            configs: path.resolve(__dirname, 'src/configs/'),
            containers: path.resolve(__dirname, 'src/containers/'),
            theme: path.resolve(__dirname, 'src/theme/')
        }
    },
    devServer: {
        historyApiFallback: true,
        contentBase: __dirname + '/build',
        port: 3002
    },
    module: {
        rules: [
            {
                loader: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            },
            {
                test: /\.scss/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                           importLoaders: 1,
                           modules: true,
                           localIdentName: '[name]__[local]__[hash:base64:5]',
                           camelCase: 'dashesOnly'
                        }
                    },
                    'sass-loader',
                ]
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'images/'
                }
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(pathsToClean),
        new webpack.HashedModuleIdsPlugin(),
        new HtmlWebpackPlugin({
            template:'./src/index.html'
        }),
        new GenerateSW({
            clientsClaim: true,
            skipWaiting: true
        })
    ]
}
