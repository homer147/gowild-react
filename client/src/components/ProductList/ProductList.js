import React from 'react';
import { Link } from 'react-router-dom';

import styles from './productList.scss';

import {black, red} from '../../configs/globalStyles';

const ProductList = ({products}) => (
    <div className={styles.productTile}>
        {products && products.map(product => (
            <div className={styles.ContentContainer} key={product.id}>
                <Link className={styles.productLink} to={`/hire/castles/${product.slug}`}>
                    <img
                        src={`/images/${product.images.thumbnail.name}`}
                        alt={`${product.title} jumping castle`}
                        className={styles.productImage}
                    />
                </Link>
                <Link className={styles.productLink} to={`/hire/castles/${product.slug}`}>
                    <h3>{product.title}</h3>
                </Link>
            </div>
        ))}
    </div>
);

export default ProductList;
