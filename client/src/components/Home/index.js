import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { sanitize } from 'dompurify';
import classnames from 'classnames';

import Hero from './Hero';
import { getHomeContent } from 'actions';
import styles from './styles.scss';

class Home extends Component {
    componentDidMount() {
        this.props.getHomeContent();
    }

    renderFlipCard(card) {
        return (
            <div className={styles.flipCardContainer} key={card.id}>
                <div className={styles.flipCard}>
                    <div className={styles.flipCardFront}>
                        <img src={`/images/${card.image.name}`} alt={card.title} />
                        <h3>{card.title}</h3>
                    </div>
                    <div className={styles.flipCardBack}>
                        <Link to={card.link} dangerouslySetInnerHTML={{__html: sanitize(card.content)}}></Link>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { flipcards } = this.props.content;
        const featureAddClass = classnames(styles.feature, styles.featureAdd);
        const largeFeatureClass = classnames(styles.feature, styles.featureLarge);

        return (
            <div className={styles.home}>
                <Hero />
                <div className={styles.content}>
                    <div className={featureAddClass}></div>
                    {flipcards ? flipcards.map(card => this.renderFlipCard(card)) : null}
                    <div className={largeFeatureClass}></div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = state => {
    return {
        content: state.home.content
    }
};

export default connect(mapStateToProps, { getHomeContent })(Home);
