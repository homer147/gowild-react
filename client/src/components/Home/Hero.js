import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import classnames from 'classnames';

import styles from './styles.scss';
import Modal from '../Modal';
import { toggleContactButton, resetContactButton } from 'actions';

class Hero extends Component {
    componentWillUnmount() {
        this.props.resetContactButton();
    }

    onContactClick = () => {
        this.props.toggleContactButton();
    }

    render() {
        const { popupActive, hero } = this.props;
        const contactButtonClass = classnames(styles.contactButton, {
            [styles.active]: popupActive
        })

        return (
            <div className={styles.hero}>
                <h2>{hero ? hero.title : ''}</h2>
                <p>{hero ? hero.content : ''}</p>
                <div className={contactButtonClass}>
                    <button onClick={this.onContactClick}>Contact Us</button>
                </div>
                <Modal
                    isOpen={popupActive}
                    onClickClose={this.onContactClick}
                >
                    <div>
                        <a href="tel:+61-0450-951-939">0450 951 939</a>
                    </div>
                    or
                    <Link to="/contact">Email</Link>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        popupActive: state.home.isContactButtonActive,
        hero: state.home.content.hero
    };
}

export default connect(mapStateToProps, {
    toggleContactButton,
    resetContactButton
})(Hero);
