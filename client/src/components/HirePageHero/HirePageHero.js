import React, { Component } from 'react';
import { sanitize } from 'dompurify';
import styled from 'styled-components';

import {black, red} from 'configs/globalStyles';
import styles from './HirePageHero.scss';

const HirePageHero = ({data}) => (
    <React.Fragment>
        {data && (
            <div className={styles.heroContainer}>
                <h2 className={styles.heading}>{data.title}</h2>
                <div>
                    <img
                        className={styles.image}
                        alt={data.title}
                        src={`/images/${data.image.name}`}
                    />
                    <div
                        className={styles.heroBlurb}
                        dangerouslySetInnerHTML={{__html: sanitize(data.content)}}
                    />
                </div>
            </div>
        )}
    </React.Fragment>
);

export default HirePageHero;
