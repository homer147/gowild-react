import React from 'react';

import styles from './styles.scss';

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <div className={styles.container}>
                Phone - <a href="tel:+61-0450-951-939">0450 951 939</a>
            </div>
        </footer>
    );
};

export default Footer;
