import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';

import styles from './styles.scss';
import { toggelMobileMenu, toggleSubMenu } from 'actions';

class Header extends Component {
    onMenuClick = () => {
        this.props.toggelMobileMenu();
    }

    toggleSubMenu = () => {
        this.props.toggleSubMenu();
    }

    renderSubMenu() {
        return (
            <ul className={styles.ddMenu}>
                <li className={styles.ddItem}><Link to="/hire/castles">Castles</Link></li>
                <li className={styles.ddItem}><Link to="/hire/slushie">Slushies</Link></li>
                <li className={styles.ddItem}><Link to="/hire/floss-popcorn">Floss/Popcorn</Link></li>
            </ul>
        );
    }

    render() {
        const { isMobileMenuOpen, isSubMenuOpen } = this.props;
        const mobileMenuClass = classnames(styles.mobileMenu, {
            [styles.active]: isMobileMenuOpen
        });
        const menuItemClass = classnames(styles.menuItem, {
            [styles.active]: isSubMenuOpen
        });

        return (
            <header className={styles.header}>
                <nav className={styles.container}>
                    <div className={mobileMenuClass} onClick={this.onMenuClick}>
                        <FontAwesomeIcon icon={faBars} />
                    </div>
                    <ul className={styles.menuList}>
                        <li className={styles.menuItem}><Link to="/">Home</Link></li>
                        <li className={styles.menuItem}><Link to="/about">About</Link></li>
                        <li>
                            <h1 className={styles.logo}>
                                <span>GoWild Jumping Castle Party Hire Brisbane</span>
                            </h1>
                        </li>
                        <li className={menuItemClass} onClick={this.toggleSubMenu}>
                            <span>Hire</span>
                            {this.renderSubMenu()}
                        </li>
                        <li className={styles.menuItem}><Link to="/contact">Contact</Link></li>
                    </ul>
                    <a
                        href="https://www.facebook.com/gowildcastles/"
                        title="Link to facebook"
                        target="_blank"
                        className={styles.facebook}
                    >
                        <FontAwesomeIcon icon={faFacebookF} />
                    </a>
                </nav>
            </header>
        );
    }
};

const mapStateToProps = state => {
    return {
        isMobileMenuOpen: state.menu.isMobileMenuOpen,
        isSubMenuOpen: state.menu.isSubMenuOpen
    }
}

export default connect(mapStateToProps, { toggelMobileMenu, toggleSubMenu })(Header);
