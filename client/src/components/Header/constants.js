export default {
    headerHeight: '130px',
    fbStyles: {
        fontSize: '38px',
        right: '50px',
        top: '46px'
    },
    logoStyles: {
        width: '215px',
        height: '200px',
        textIndent: '-3333px',
        background: '/images/GoWild.png'
    }
}