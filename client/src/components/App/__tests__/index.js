import React from 'react';
import { shallow } from 'enzyme';

import App from '../index';
import Header from '../../Header';
import Footer from '../../Footer';

let wrapped;

describe('App', () => {
    beforeEach(() => {
        wrapped = shallow(<App />);
    });

    it('should render the Header once', () => {
        expect(wrapped.find(Header).length).toEqual(1);
    });

    it('should render the Footer once', () => {
        expect(wrapped.find(Footer).length).toEqual(1);
    });
});
