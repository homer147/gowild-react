import React from 'react';
import { Router, Route } from 'react-router-dom';

import Header from '../Header';
import Footer from '../Footer';
import Home from '../Home';
import About from '../About';
import Hire from 'containers/Hire/Hire';
import Contact from '../Contact';
import history from '../../history';
import styles from './styles.scss';

const App = () => {
    return (
        <React.Fragment>
            <Router history={history}>
                <div className={styles.wrapper}>
                    <Header />
                    <Route path="/" exact component={Home} />
                    <Route path="/about" component={About} />
                    <Route path="/hire" component={Hire} />
                    <Route path="/contact" component={Contact} />
                    <Footer />
                </div>
            </Router>
        </React.Fragment>
    );
}

export default App;
