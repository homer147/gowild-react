import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import classnames from 'classnames';

import styles from './styles.scss';

const FIELDS = [
    {
        name: 'firstName',
        label: 'First name',
        type: 'text'
    },
    {
        name: 'lastName',
        label: 'Last name',
        type: 'text'
    },
    {
        name: 'phone',
        label: 'Phone number',
        type: 'tel'
    },
    {
        name: 'email',
        label: 'Email',
        type: 'email',
        placeholder: 'youremail@yourdomain.com'
    },
    {
        name: 'message',
        label: 'Message',
        type: 'textarea',
        placeholder: 'Enter your message here'
    }
]

class Contact extends Component {
    renderField(field) {
        const { meta: { touched, error } } = field;
        const inputField = classnames({
            [styles.error]: error && touched
        });

        return (
            <div className={styles.field}>
                <label>{field.label}</label>
                {field.type === 'textarea' ? (
                    <textarea
                        rows="5"
                        placeholder={field.placeholder}
                        className={inputField}
                        {...field.input}
                    ></textarea>
                ) : (
                    <input
                        type={field.type}
                        placeholder={field.placeholder ? field.placeholder : field.label}
                        className={inputField}
                        {...field.input}
                    />
                )}
                <div className={styles.errorMessage}>
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    onSubmit(values) {
        console.log('form submitted');
    }

    render() {
        const { handleSubmit, submitting } = this.props;
        const contactClass = classnames(styles.contact, 'container', 'content'); // TODO mixin these classes

        return (
            <div className={contactClass}>
                <section>
                    <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                        <h3>Have a question?</h3>
                        <p className={styles.heading}>Get in touch!</p>
                        {FIELDS.map(field => (
                            <Field
                                key={field.name}
                                name={field.name}
                                type={field.type}
                                label={field.label}
                                placeholder={field.placeholder}
                                component={this.renderField}
                            />
                        ))}
                        <button type="submit" disabled={submitting}>
                            Send
                        </button>
                    </form>
                </section>
            </div>
        );
    }
};

function validate(values) {
    const errors = {};

    FIELDS.map(field => {
        const { name, label, type } = field;

        if (!values[name]) {
            if (type === 'textarea') {
                errors[name] = 'Please enter a message of at least 50 Characters';
            } else {
                errors[name] = `Please enter your ${label.toLowerCase()}`;
            }
        }
    });

    return errors;
}

export default reduxForm({
    validate,
    form: 'contactForm'
})(connect()(Contact));
