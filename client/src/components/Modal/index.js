import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import styles from './styles.scss';

class Modal extends Component {
    handleModalClick = e => {
        e.stopPropagation();
    }

    handleClickClose = e => {
        this.props.onClickClose();
    }

    render() {
        if(this.props.isOpen) {
            return (
                <div className={styles.modal} onClick={this.handleClickClose}>
                    <div className={styles.modalBody} onClick={this.handleModalClick}>
                        <span className={styles.close}>
                            <FontAwesomeIcon icon={faTimes} onClick={this.handleClickClose} />
                        </span>
                        {this.props.children}
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
}

export default Modal;
