import React, { Component } from 'react';
import { connect } from 'react-redux';

import HirePageHero from 'components/HirePageHero/HirePageHero';
import ProductList from 'components/ProductList/ProductList';
import { getCastlesData } from 'actions';
import styles from './hire.scss';

class Castles extends Component {
    componentDidMount() {
        this.props.getCastlesData();
    }

    render() {
        const { content } = this.props

        return (
            <div className={styles.hireContent}>
                <HirePageHero data={content.hero} />
                <ProductList products={content.castles} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        content: state.castles.content
    };
}

export default connect(mapStateToProps, { getCastlesData })(Castles);
