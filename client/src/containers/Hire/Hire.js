import React, { Component } from 'react';
import { matchPath } from 'react-router-dom';

import Castles from 'containers/Hire/Castles';

class Hire extends Component {
    renderPageContent() {
        const path = this.props.location;

        if (path.pathname.includes('castles')) {
            return (<Castles />);
        } else {
            return (<div>Hire</div>);
        }
    }

    render() {
        return (
            <div className="hire">
                {this.renderPageContent()}
            </div>
        );
    }
};

export default Hire;
