import api from '../api';
import {
    TOGGLE_MOBILE_MENU,
    TOGGLE_SUB_MENU,
    TOGGLE_CONTACT_BUTTON,
    RESET_CONTACT_BUTTON,
    GET_HOME_CONTENT,
    GET_CASTLES_DATA
} from './types';

export const toggelMobileMenu = () => {
    return {
        type: TOGGLE_MOBILE_MENU
    }
};

export const toggleSubMenu = () => {
    return {
        type: TOGGLE_SUB_MENU
    }
};

export const toggleContactButton = () => {
    return {
        type: TOGGLE_CONTACT_BUTTON
    }
};

export const resetContactButton = () => {
    return {
        type: RESET_CONTACT_BUTTON
    }
};

export const getHomeContent = () => async dispatch => {
    const response = await api.get('/getHomeContent');

    dispatch({
        type: GET_HOME_CONTENT,
        payload: response.data
    });
};

export const getCastlesData = () => async dispatch => {
    const response = await api.get('/getCastlesData');

    dispatch({
        type: GET_CASTLES_DATA,
        payload: response.data
    });
};
