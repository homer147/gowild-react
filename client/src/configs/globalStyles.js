// colours
export const black = '#333';
export const red = '#ec3042';
export const white = '#fff';

// dimentions
export const headerHeight = '130px';
export const fbStyles = {
    fontSize: '38px',
    right: '50px',
    top: '46px'
};
