import {
    GET_CASTLES_DATA
} from '../actions/types';

const INITIAL_STATE = {
    content: {}
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case GET_CASTLES_DATA:
            return { ...state, content: action.payload };

        default:
            return state;
    }
};
