import {
  TOGGLE_MOBILE_MENU,
  TOGGLE_SUB_MENU
} from '../actions/types';

const INITIAL_STATE = {
  isMobileMenuOpen: false,
  isSubMenuOpen: false
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case TOGGLE_MOBILE_MENU:
            return { ...state, isMobileMenuOpen: !state.isMobileMenuOpen };

        case TOGGLE_SUB_MENU:
            return { ...state, isSubMenuOpen: !state.isSubMenuOpen };

        default:
            return state;
    }
};
