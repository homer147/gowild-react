import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import menuReducer from './menuReducer';
import homeReducer from './homeReducer';
import castlesReducer from './castlesReducer';

export default combineReducers({
    menu: menuReducer,
    home: homeReducer,
    castles: castlesReducer,
    form: formReducer
});
