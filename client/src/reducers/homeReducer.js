import {
    TOGGLE_CONTACT_BUTTON,
    RESET_CONTACT_BUTTON,
    GET_HOME_CONTENT
} from '../actions/types';

const INITIAL_STATE = {
    isContactButtonActive: false,
    content: {}
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case TOGGLE_CONTACT_BUTTON:
            return { ...state, isContactButtonActive: !state.isContactButtonActive };

        case RESET_CONTACT_BUTTON:
            return { ...state, isContactButtonActive: false };

        case GET_HOME_CONTENT:
            return { ...state, content: action.payload };

        default:
            return state;
    }
};
