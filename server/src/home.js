import axios from 'axios';
import showdown from 'showdown';

const converter = new showdown.Converter();

const getHomeContent = async function (error, response) {
    const homeContent = {};
    const [ hero, flipcards ] = await Promise.all([
        axios.get('http://localhost:1337/heroes?name=home'),
        axios.get('http://localhost:1337/flipcards')
    ]);

    homeContent.hero = parseHero(hero);
    homeContent.flipcards = parseFlipCards(flipcards);

    response.send(homeContent);
};

function parseHero(res) {
    return res.data[0];
};

function parseFlipCards(res) {
    const cards = res.data.map(card => {
        const content = converter.makeHtml(card.content);
        card.content = content;

        return card;
    });

    return cards;
};

module.exports = {
    getHomeContent: getHomeContent
};
