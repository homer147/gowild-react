import axios from 'axios';
import showdown from 'showdown';

const converter = new showdown.Converter();

const parseHero = function (res) {
    const hero = res.data[0];
    const heroContent = converter.makeHtml(hero.content);
    hero.content = heroContent

    return hero;
};

const parseCastles = function (res) {
    const castles = res.data;

    const newCastles = castles.map(castle => {
        castle.copy = {
            body: converter.makeHtml(castle.copyBody),
            keyInfo: castle.copyKeyInfo,
            packages: castle.copyPackages
        }

        castle.images = {
            main: castle.mainImage,
            thumbnail: castle.thumbnailImage,
            gallery: castle.galleryImages
        }

        delete castle.copyBody;
        delete castle.copyKeyInfo;
        delete castle.copyPackages;
        delete castle.mainImage;
        delete castle.thumbnailImage;
        delete castle.galleryImages;

        return castle;
    });

    return newCastles;
};

const getCastlesData = async function (error, response) {
    const castlesData = {};
    const [hero, castles] = await Promise.all([
        axios.get('http://localhost:1337/heroes?name=castles'),
        axios.get('http://localhost:1337/castles')
    ]);

    castlesData.hero = parseHero(hero);
    castlesData.castles = parseCastles(castles);

    response.send(castlesData);
};

module.exports = {
    getCastlesData: getCastlesData
};
