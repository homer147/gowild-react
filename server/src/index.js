'use strict';

import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import 'dotenv/config';
import home from './home';
import castles from './castles';

const app = express();

app.use(cors({
    "origin": process.env.CORSORIGIN,
    "methods": process.env.CORSMETHODS
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/api/getHomeContent', home.getHomeContent);
app.get('/api/getCastlesData', castles.getCastlesData);

/*app.post('/', (req, res) => {
    return res.send('Received a POST HTTP method');
});

app.put('/', (req, res) => {
    return res.send('Received a PUT HTTP method');
});

app.delete('/', (req, res) => {
    return res.send('Received a DELETE HTTP method');
});*/

app.listen(process.env.PORT, () => {
    console.log(`Now listening on port ${process.env.PORT}`);
});
